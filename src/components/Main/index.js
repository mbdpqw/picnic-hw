import React from 'react';
import { Link } from 'react-router-dom';

const Main = () => (
  <div className="text-center">
    <Link to="/list">
      <h2>
        Go to List Page
      </h2>
    </Link>
  </div>
)

export default Main;

