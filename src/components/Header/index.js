import React from 'react'

const Header = () => (
  <header>
    <div 
      className="text-center my-4">
      <h1>Pique-nique</h1>
    </div>
  </header>
)

export default Header