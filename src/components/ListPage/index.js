import React, { Component } from 'react';
import { getAllProducts } from '../Api';
import SearchBox from './SearchBox';
import { Link } from 'react-router-dom';

import {Container, 
        Row, 
        Col, 
        Image} 
        from 'react-bootstrap';

import '../../custom.css';

class ListPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allProducts: [],
      error: null,
      isLoading: true,
      input: '',
    }
  }

  componentDidMount = async () => {
    try {
      const result = await getAllProducts();
      this.setState(() => ({ allProducts: result.data.products, isLoading: false }))
    } catch(error) {
      this.setState(() => ({ error: error, isLoading: false }));
    }
  }

  handleInputChange = (input) => {
    this.setState({input: input})
  }

  render () {
    const { allProducts, input } = this.state;
    const filteredProducts = input 
                                ? allProducts.filter(product => product.name.toLowerCase().startsWith(input.toLowerCase())) 
                                : allProducts;

    return (
      <Container>
        <Row className="justify-content-center">
            <SearchBox 
              handleInputChange={this.handleInputChange} 
              input={input}/>
        </Row>
        <Row className="text-center">
          {filteredProducts.map(product => (
              <Col 
                key={product.product_id}
                xs={6} sm={6} md={4} lg={2}
                className="list-item"
                >
                <Link 
                  to={'/list/' + product.product_id}
                  style={{textDecoration: 'none'}}
                  >
                  <Image 
                    className="list-image py-3"
                    src={product.image} />
                  <div className="pb-3">
                  <div>
                    <span>{product.name}</span>
                  </div>
                  <div>
                    <h3>{"€ " + product.price + ".00"}</h3>
                  </div>
                  </div>
                </Link>
              </Col>
            ))
          }
        </Row>
      </Container>
    )
  }
  
}

export default ListPage;