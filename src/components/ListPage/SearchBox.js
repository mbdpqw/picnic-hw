import React from 'react';

const SearchBox = ({input, handleInputChange}) => {

  const handleChange = (event) => {
    const value = event.target.value;
    handleInputChange(value)
  }

  return (
    <div className="pb-5">
      <input 
        type="text" 
        placeholder="Search" 
        value={input}
        onChange={handleChange}/>
    </div>
  )
}

export default SearchBox;

