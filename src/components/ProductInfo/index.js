import React, { Component } from 'react';
import { Col, Row, Container, Image } from 'react-bootstrap';
import { getProduct } from '../Api';
import withModal from './withModal';

class ProductInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productInfo: [],
      isLoading: true,
      error: null,
    }
  }

  componentDidMount = async () => {
    try {
      const {data} = await getProduct(this.props.match.params.productID);
      this.setState(() => ({ productInfo: data, isLoading: false }))
    } catch(error) {
      this.setState(() => ({ error: error, isLoading: false }));
    }
  }

  render() {
    if (!this.state.productInfo) {
      return null;
    }

    const { image, name, price, description} = this.state.productInfo;

    return (
          <Container>
            <Row>
              <Col
                sm={12} md={6}
                className="text-center"
                >
                <Image fluid src={image}/>
              </Col>
              <Col
                sm={12} md={6}
                >
                <h1>
                  {name}
                </h1>
                <p>
                  {description}
                </p>
                <h2>
                  {"€ " + price + ".00"}
                </h2>
              </Col>
            </Row>
          </Container>
    )
  }
}

export default ProductInfo;
export { withModal };