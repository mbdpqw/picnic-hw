import React from 'react';
import { Modal } from 'react-bootstrap';

function withModal (Component) {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        showModal: true
      }
    }

    handleClose = () => {
      this.setState(() => ({showModal: false}))
      this.props.history.push("/list");
    }

    render() {
      const { showModal } = this.state;

      return (
        <Modal 
          show={showModal} 
          onHide={this.handleClose}
          size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Product Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Component {...this.props} />
          </Modal.Body>
        </Modal>
      )
    }
  }
}

export default withModal;
