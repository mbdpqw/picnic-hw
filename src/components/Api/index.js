import axios from 'axios';

const BASE_URL = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart";

export const getAllProducts = async () => {
  const req = {
    method: "get",
    baseURL: BASE_URL,
    url: "/list"
  }

  const result = await axios(req);

  return result;
}

export const getProduct = async (product_id) => {
  const req = {
    method: "get",
    baseURL: BASE_URL,
    url: "/" + product_id + "/detail"
  }

  const result = await axios(req);

  return result;
}
