import React, { Component } from 'react';
import '../../custom.css';

import { Route, Switch } from 'react-router-dom';
import ListPage from '../ListPage';
import ProductInfo, { withModal } from '../ProductInfo';
import Header from '../Header';
import Main from '../Main';

class App extends Component {
  constructor() {
    super();
    this.state = {
      width: window.innerWidth,
    };
  }
  
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }
  
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const { width } = this.state;
    const isMobile = width < 768;

    return (
      <div>
        <Header />
        {
          isMobile
          ?
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/list" component={ListPage} />
            <Route path="/list/:productID" component={ProductInfo} />
          </Switch>
          :
          <>
            <Route exact path="/" component={Main} />
            <Route path="/list" component={ListPage} />
            <Route path="/list/:productID" component={withModal(ProductInfo)} />
          </>
        }
      </div>
    )
  }
}

export default App;
